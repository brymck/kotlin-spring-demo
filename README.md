kotlin-spring-demo
==================

This is just a test app to demonstrate usage of

* [Kotlin](https://kotlinlang.org/) as a language
* [GitLab](https://about.gitlab.com/) <abbr title="Continuous integration  and continuous deployment">CI/CD</abbr>
* [Gradle](https://gradle.org/) as the build system
* [Spring Boot](https://spring.io/projects/spring-boot) as the server framework
* [Sonar](https://www.sonarqube.org/) for static analysis

```
git clone git@gitlab.com:brymck/kotlin-spring-demo.git
cd kotlin-spring-demo
gradle build
java -jar build/libs/kotlin-spring-demo.jar
```