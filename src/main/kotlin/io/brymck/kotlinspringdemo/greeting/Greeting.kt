package io.brymck.kotlinspringdemo.greeting

data class Greeting(val id: Long, val content: String)
