package io.brymck.kotlinspringdemo.greeting

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GreetingControllerTest(@Autowired val restTemplate: TestRestTemplate) {
    @Test
    fun `default greeting returns successfully`() {
        val entity = restTemplate.getForEntity<Greeting>("/greeting")
        assertEquals(HttpStatus.OK, entity.statusCode)
        assertEquals("Hello, world!", entity.body.content)
    }

    @Test
    fun `named greeting returns successfully`() {
        val entity = restTemplate.getForEntity<Greeting>("/greeting?name=Bryan")
        assertEquals(HttpStatus.OK, entity.statusCode)
        assertEquals("Hello, Bryan!", entity.body.content)
    }
}