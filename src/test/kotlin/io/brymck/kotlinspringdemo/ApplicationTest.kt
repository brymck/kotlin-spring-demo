package io.brymck.kotlinspringdemo

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ApplicationTest {
    @Test
    fun `context loads`() {
        main(emptyArray())
    }
}